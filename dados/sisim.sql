SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `sisim` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `sisim` ;

-- -----------------------------------------------------
-- Table `sisim`.`Simulado`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisim`.`Simulado` (
  `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificação' ,
  `Titulo` VARCHAR(150) NOT NULL COMMENT 'Nome' ,
  `Versao` INT NOT NULL COMMENT 'Versão' ,
  `Cadastro` DATETIME NOT NULL COMMENT 'Data/Hora do Cadastro' ,
  `Pontuacao` INT NOT NULL COMMENT 'Pontuação Mínima' ,
  `Duracao` INT NOT NULL COMMENT 'Duração' ,
  `Questoes` INT NOT NULL COMMENT 'Quantidade de Questões' ,
  `Inicio` DATETIME NOT NULL COMMENT 'Data/Hora do Início' ,
  `Fim` DATETIME NOT NULL COMMENT 'Data/Hora do Fim' ,
  `ExibirDescricaoInicio` ENUM('S', 'N') NOT NULL DEFAULT 'N' COMMENT 'Exibir a descrição no início do simulado?' ,
  `Descricao` TEXT NULL COMMENT 'Descrição' ,
  `Situacao` ENUM('L', 'N', 'F') NOT NULL DEFAULT 'N' COMMENT 'Situação: Liberado, Não Liberado ou Finalizado' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
COMMENT = 'Simulados';

USE `sisim` ;
USE `sisim`;

DELIMITER $$
USE `sisim`$$


CREATE TRIGGER `Simulado_BINS` BEFORE INSERT ON Simulado FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
	SET NEW.Titulo = TRIM(NEW.Titulo);
	SET NEW.Cadastro = NOW();
END$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
