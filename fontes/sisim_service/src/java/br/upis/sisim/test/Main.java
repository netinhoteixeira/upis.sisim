/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upis.sisim.test;

import br.upis.sisim.dao.PessoaNivel;
import br.upis.sisim.persistence.HibernateUtil;
import br.upis.sisim.pojo.Pessoa;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author francisco
 */
public class Main {

    public static void main(String[] params) {
        Pessoa retorno = new Pessoa();

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(br.upis.sisim.persistence.Pessoa.class);
        criteria.add(Restrictions.like("usuario", "francisco"));
        criteria.add(Restrictions.like("senha", "12345"));
        List<br.upis.sisim.persistence.Pessoa> pessoas = criteria.list();
        if (!pessoas.isEmpty()) {
            for (br.upis.sisim.persistence.Pessoa pessoa : pessoas) {
                retorno.setAutorizado(true);
                retorno.setNome(pessoa.getNome());
                retorno.setNivel(PessoaNivel.valueOf(pessoa.getPessoaNivel().getNome()));
                break;
            }
        }
        
        System.out.println(retorno.isAutorizado());

        System.out.println("Fim");
    }
}
