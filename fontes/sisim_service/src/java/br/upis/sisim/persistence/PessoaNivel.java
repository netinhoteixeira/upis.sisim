package br.upis.sisim.persistence;
// Generated May 29, 2013 8:38:07 PM by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * PessoaNivel generated by hbm2java
 */
public class PessoaNivel  implements java.io.Serializable {


     private char id;
     private String nome;
     private Set pessoas = new HashSet(0);

    public PessoaNivel() {
    }

	
    public PessoaNivel(char id) {
        this.id = id;
    }
    public PessoaNivel(char id, String nome, Set pessoas) {
       this.id = id;
       this.nome = nome;
       this.pessoas = pessoas;
    }
   
    public char getId() {
        return this.id;
    }
    
    public void setId(char id) {
        this.id = id;
    }
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    public Set getPessoas() {
        return this.pessoas;
    }
    
    public void setPessoas(Set pessoas) {
        this.pessoas = pessoas;
    }




}


