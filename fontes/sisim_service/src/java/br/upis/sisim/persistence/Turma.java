package br.upis.sisim.persistence;
// Generated May 29, 2013 8:38:07 PM by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * Turma generated by hbm2java
 */
public class Turma  implements java.io.Serializable {


     private Long id;
     private String nome;
     private Set pessoas = new HashSet(0);

    public Turma() {
    }

    public Turma(String nome, Set pessoas) {
       this.nome = nome;
       this.pessoas = pessoas;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    public Set getPessoas() {
        return this.pessoas;
    }
    
    public void setPessoas(Set pessoas) {
        this.pessoas = pessoas;
    }




}


