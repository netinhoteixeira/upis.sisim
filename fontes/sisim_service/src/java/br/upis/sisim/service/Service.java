/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upis.sisim.service;

import br.upis.sisim.dao.PessoaNivel;
import br.upis.sisim.persistence.Disciplina;
import br.upis.sisim.persistence.HibernateUtil;
import br.upis.sisim.persistence.Turma;
import br.upis.sisim.pojo.Pessoa;
import br.upis.sisim.pojo.PessoaArea;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author francisco
 */
@WebService(serviceName = "Service")
public class Service {

    /**
     * Verifica o usuário e senha informados e retorna suas informações.
     *
     * @param usuario Usuário
     * @param senha Senha
     * @return Pessoa
     */
    @WebMethod(operationName = "acessar")
    public Pessoa acessar(@WebParam(name = "usuario") String usuario, @WebParam(name = "senha") String senha) {
        Pessoa retorno = new Pessoa();

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Criteria criteria = session.createCriteria(br.upis.sisim.persistence.Pessoa.class);
            criteria.add(Restrictions.like("usuario", usuario));
            criteria.add(Restrictions.like("senha", senha));
            List<br.upis.sisim.persistence.Pessoa> pessoas = criteria.list();
            if (!pessoas.isEmpty()) {
                for (br.upis.sisim.persistence.Pessoa pessoa : pessoas) {
                    retorno.setAutorizado(true);
                    retorno.setUsuario(usuario);
                    retorno.setNome(pessoa.getNome());
                    retorno.setNivel(PessoaNivel.valueOf(pessoa.getPessoaNivel().getNome()));

                    if (PessoaNivel.valueOf(pessoa.getPessoaNivel().getNome()) == PessoaNivel.Aluno) {
                        retorno.setAcesso("Turma");

                        Iterator i = pessoa.getTurmas().iterator();
                        while (i.hasNext()) {
                            Turma turma = (Turma) i.next();
                            retorno.getArea().add(new PessoaArea(turma.getId(), turma.getNome()));
                        }
                    } else if (PessoaNivel.valueOf(pessoa.getPessoaNivel().getNome()) == PessoaNivel.Professor) {
                        retorno.setAcesso("Disciplina");

                        Iterator i = pessoa.getDisciplinas().iterator();
                        while (i.hasNext()) {
                            Disciplina disciplina = (Disciplina) i.next();
                            retorno.getArea().add(new PessoaArea(disciplina.getId(), disciplina.getNome()));
                        }
                    }

                    break;
                }
            }

            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }

            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }

        return retorno;
    }

    @WebMethod(operationName = "disciplinas")
    public List<PessoaArea> disciplinas() {
        List<PessoaArea> retorno = new ArrayList<PessoaArea>();

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Criteria criteria = session.createCriteria(br.upis.sisim.persistence.Disciplina.class);
            List<br.upis.sisim.persistence.Disciplina> disciplinas = criteria.list();
            if (!disciplinas.isEmpty()) {
                for (br.upis.sisim.persistence.Disciplina disciplina : disciplinas) {
                    retorno.add(new PessoaArea(disciplina.getId(), disciplina.getNome()));
                }
            }

            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }

            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }

        return retorno;
    }

    @WebMethod(operationName = "turmas")
    public List<PessoaArea> turmas() {
        List<PessoaArea> retorno = new ArrayList<PessoaArea>();

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Criteria criteria = session.createCriteria(br.upis.sisim.persistence.Turma.class);
            List<br.upis.sisim.persistence.Turma> turmas = criteria.list();
            if (!turmas.isEmpty()) {
                for (br.upis.sisim.persistence.Turma turma : turmas) {
                    retorno.add(new PessoaArea(turma.getId(), turma.getNome()));
                }
            }

            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }

            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }

        return retorno;
    }
}