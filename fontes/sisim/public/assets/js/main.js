/**
 * Adiciona ao jQuery Validator o método de validação para as datas.
 */
$.validator.addMethod('greaterThan',
    function (value, element, params) {
        var valid, startDate, endDate;

        // initialize
        startDate = $(params[0]).val();
        endDate = value;

        if ((startDate.length > 0) && (endDate.length > 0)) {
            // convert from pt-br
            startDate = startDate.split(' ');
            startDate[0] = startDate[0].split('/');
            startDate = startDate[0][1] + '/' + startDate[0][0] + '/' + startDate[0][2] + ' ' + startDate[1];

            // end date
            endDate = endDate.split(' ');
            endDate[0] = endDate[0].split('/');
            endDate = endDate[0][1] + '/' + endDate[0][0] + '/' + endDate[0][2] + ' ' + endDate[1];

            valid = !/Invalid|NaN/.test(new Date(endDate));
            if (valid) {
                return new Date(endDate) > new Date(startDate);
            }

            valid = isNaN(endDate) && isNaN(startDate) ||
                (Number(endDate) > Number(startDate));

            return valid;
        } else {
            return false;
        }
    }, 'Precisa ser maior que {1}.');//'Must be greater than {0}.');

/**
 * Adiciona o método no jQuery para verificar se o ENTER foi pressionado.
 * @param fnc Função que será chamada
 */
$.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                fnc.call(this, ev);
            }
        })
    })
};

/**
 * Aplica o tema atual aos painel fornecido. O mesmo é composto
 * @param id
 */
function themePanel(id) {
    var header, headerText, body;

    $(id).addClass('jqx-rc-all jqx-rc-all-bootstrap jqx-window jqx-window-bootstrap jqx-popup jqx-popup-bootstrap jqx-widget jqx-widget-bootstrap jqx-widget-content jqx-widget-content-bootstrap')
        .css('width', '100%')
        .css('outline', 'none')
        .css('margin-top', '.5em')
        .css('position', 'static');

    header = $('div:first', id).addClass('jqx-window-header jqx-window-header-bootstrap jqx-widget-header jqx-widget-header-bootstrap jqx-disableselect jqx-disableselect-bootstrap jqx-rc-t jqx-rc-t-bootstrap')
        .css('width', '100%')
        .css('position', 'relative')
        .css('cursor', 'default');
    headerText = header.text();

    header.html(
        $('<div />')
            .css('float', 'left')
            .css('direction', 'ltr')
            .css('margin-top', '0px')
            .text(headerText)
    );

    body = $('div:last', id).addClass('jqx-window-content jqx-window-content-bootstrap jqx-widget-content jqx-widget-content-bootstrap jqx-rc-b jqx-rc-b-bootstrap')
        .css('width', '100%')
        .css('cursor', 'default');
}

/**
 * Visualmente define os campos que são obrigatórios.
 */
function definirComoObrigatorio() {
    $('.obrigatorio').each(function () {
        if ($(this).text().indexOf('*') === -1) {
            $(this)
                .attr('title', 'Este campo é requerido')
                .append(' <span class="obrigatorio">&#42;</span>&nbsp;');
        }
    });
}

/**
 * Twitter Bootstrap Growl
 */
function notificar(mensagem, tipo) {
    $.bootstrapGrowl(mensagem, {
        type: tipo,
        width: 'auto',
        allow_dismiss: false,
        delay: 1000
    });
}

function removerNotificacoes() {
    $('.bootstrap-growl').remove();
}

function notificarInformacao(mensagem) {
    notificar(mensagem, 'info');
}

function notificarSucesso(mensagem) {
    notificar(mensagem, 'success');
}

function notificarErro(mensagem) {
    notificar(mensagem, 'error');
}

function removerNotificacoesErro() {
    $('.bootstrap-growl.alert.alert-error').remove();
}

/**
 * jQuery qTip2 Growl
 * Usage: createGrowl(titulo, mensagem, false);
 */
$(document).ready(function () {
    /*
     $('button').click(function() {
     // Check if it should be persistent (can set to a normal bool if you like!)
     createGrowl($(this).hasClass('persistent'));
     });
     */
    window.createGrowl = function (title, message, persistent) {
        // Use the last visible jGrowl qtip as our positioning target
        var target = $('.qtip.jgrowl:visible:last');

        // Create your jGrowl qTip...
        $(document.body).qtip({
            // Any content config you want here really.... go wild!
            content: {
                text: message,//'Testing out our jGrowl implementation (persistent: ' + persistent + ')',
                title: {
                    text: title//,'Attention!',
                    //button: true
                }
            },
            position: {
                my: 'top right',
                // Not really important...
                at: (target.length ? 'bottom' : 'top') + ' right',
                // If target is window use 'top right' instead of 'bottom right'
                target: target.length ? target : $(window),
                // Use our target declared above
                adjust: { y: 5 },
                effect: function (api, newPos) {
                    // Animate as usual if the window element is the target
                    $(this).animate(newPos, {
                        duration: 200,
                        queue: false
                    });

                    // Store the final animate position
                    api.cache.finalPos = newPos;
                }
            },
            show: {
                event: false,
                // Don't show it on a regular event
                ready: true,
                // Show it when ready (rendered)
                effect: function () {
                    $(this).stop(0, 1).fadeIn(400);

                    // Update positions
                    updateGrowls();
                },
                // Matches the hide effect
                delay: 0,
                // Needed to prevent positioning issues
                // Custom option for use with the .get()/.set() API, awesome!
                persistent: persistent
            },
            hide: {
                event: false,
                // Don't hide it on a regular event
                effect: function (api) {
                    // Do a regular fadeOut, but add some spice!
                    $(this).stop(0, 1).fadeOut(400).queue(function () {
                        // Destroy this tooltip after fading out
                        api.destroy();

                        // Update positions
                        updateGrowls();
                    });
                }
            },
            style: {
                classes: 'jgrowl qtip-shadow qtip-red qtip-rounded',
                // Some nice visual classes
                tip: false // No tips for this one (optional of course)
            },
            events: {
                render: function (event, api) {
                    // Trigger the timer (below) on render
                    timer.call(api.elements.tooltip, event);
                }
            }
        }).removeData('qtip');
    };

    // Make it a window property see we can call it outside via updateGrowls() at any point
    window.updateGrowls = function () {
        // Loop over each jGrowl qTip
        var each = $('.qtip.jgrowl'),
            width = each.outerWidth(),
            height = each.outerHeight(),
            gap = each.eq(0).qtip('option', 'position.adjust.y'),
            pos;

        each.each(function (i) {
            var api = $(this).data('qtip');

            // Set target to window for first or calculate manually for subsequent growls
            api.options.position.target = !i ? $(window) : [
                pos.left + width, pos.top + (height * i) + Math.abs(gap * (i - 1))
            ];
            api.set('position.at', 'top right');

            // If this is the first element, store its finak animation position
            // so we can calculate the position of subsequent growls above
            if (!i) {
                pos = api.cache.finalPos;
            }
        });
    };

    // Setup our timer function
    function timer(event) {
        var api, lifespan;

        api = $(this).data('qtip');
        lifespan = 5000; // 5 second lifespan

        // If persistent is set to true, don't do anything.
        if (api.get('show.persistent') === true) {
            return;
        }

        // Otherwise, start/clear the timer depending on event type
        clearTimeout(api.timer);
        if (event.type !== 'mouseover') {
            api.timer = setTimeout('$("#' + api._id + '").qtip("destroy");$("#' + api._id + '").remove();updateGrowls();', lifespan);
        }
    }

    // Utilise delegate so we don't have to rebind for every qTip!
    $(document).delegate('.qtip.jgrowl', 'mouseover mouseout', timer);
});

/**
 * Desta os resultados da procura na listagem.
 */
function destacarProcuraNaListagem(procura, listagem) {
    var regex;
    regex = new RegExp($('#' + procura).val(), 'ig');
    $('#' + listagem).highlightRegex(regex);
}

/**
 * Quando a página for carregada.
 */
$(document).ready(function () {
    $('.tip').tooltip();
});