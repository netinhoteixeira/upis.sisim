<?php
return array(
    '_root_' => 'inicio/index', // The default route
    '_404_' => 'inicio/404', // The main 404 route

    'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),

    'acessar' => 'acesso/acessar',
    'sair' => 'acesso/sair',

    'rest/cadastro/assuntos/questoes/simulado(/:any)' => 'rest/cadastro/assuntos/assuntos_simulado/$1',

    'rest/cadastro/questoes/simulado(/:any)' => 'rest/cadastro/questoes/questoes_simulado/$1',

    'relatorio/turmas(/:any)?' => 'relatorio/turmas/$1',
    'rest/cadastro/turmas/relatorio/simulado(/:any)' => 'rest/cadastro/turmas/relatorio_simulado/$1',
);