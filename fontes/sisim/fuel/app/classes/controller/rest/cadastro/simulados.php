<?php

/**
 * Class Controller_Rest_Cadastro_Simulados
 *
 * Classe que cuida do CRUD do cadastro dos simulados.
 */
class Controller_Rest_Cadastro_Simulados extends Controller_Rest
{

    private $usuario;

    /**
     * Construtor da classe.
     */
    function __construct(\Request $request) // nesse caso o construtor recebe a requisição tal qual o construtor de seu pai
    {
        // chama o construtor da classe pai
        parent::__construct($request);

        // obtém o usuário do banco
        $usuario = \Session::get('usuario');

        // obtém a pessoa
        $pessoas = Model_Pessoa::find('all', array(
            'where' => array(
                array('usuario', $usuario->usuario),
            ),
        ));

        foreach ($pessoas as $value) {
            $this->usuario = $value;
        }
    }

    /**
     * Obtém todos os registros para a listagem do seu cadastro.
     * @return array
     */
    public function get_index()
    {
        // obtém o parâmetro de pesquisa
        $procurarpor = Input::get('procurarpor');

        $onde = null;
        switch ($this->usuario->Nivel) {
            case 'Professor':
                if (!empty($procurarpor)) {
                    $onde = array(
                        'CriadoPor' => $this->usuario->id,
                        'and' => array('Titulo', 'LIKE', '%' . $procurarpor . '%'),
                    );
                } else {
                    $onde = array(
                        'CriadoPor' => $this->usuario->id
                    );
                }
                break;
            case 'Coordenador':
                if (!empty($procurarpor)) {
                    $onde = array('Titulo', 'LIKE', '%' . $procurarpor . '%');
                }
                break;
        }

        // obtém todos os registros
        if (!is_null($onde)) {
            $registros = Model_Simulado::find('all', array(
                    'where' => $onde,
                    'order_by' => array('Titulo' => 'asc'))
            );
        } else {
            $registros = Model_Simulado::find('all', array(
                    'order_by' => array('Titulo' => 'asc'))
            );
        }

        // processamento dos registros, pois eles precisam ser formatados para aproveitar a estrutura do jqxListBox
        $registros = $this->processarRegistros($registros);

        // retorna os registros encontrados e processados
        return $registros;
    }

    /**
     * Insere um novo registro.
     * @return array
     */
    public function post_index()
    {
        $simulado = Model_Simulado::forge();
        $simulado->Titulo = Input::post('titulo');
        $simulado->Versao = Input::post('versao');
        // a data do cadastro deve ser do sistema
        $simulado->Cadastro = date('Y-m-d H:i:s');
        $simulado->Pontuacao = Input::post('pontuacao');
        $simulado->Duracao = Input::post('duracao');
        $simulado->Questoes = Input::post('questoes');
        $simulado->Inicio = $this->formatarDataSQL(Input::post('inicio'));
        $simulado->Fim = $this->formatarDataSQL(Input::post('fim'));
        $simulado->ExibirDescricaoInicio = (Input::post('exibirDescricaoInicio') === 'true') ? 'S' : 'N';
        $simulado->Descricao = Input::post('descricao');
        $simulado->Situacao = 'E'; // Edição
        $simulado->CriadoPor = $this->usuario->id;
        $simulado->save();

        echo 'ok';
    }

    /**
     * Modifica um registro com a identificação fornecida.
     */
    public function put_index()
    {
        $id = Input::put('id');

        // em tese o registro é válido
        if ($id > 0) {
            // procura o registro por sua identificação
            $simulado = Model_Simulado::find($id);

            // de fato o registro é válido pois não é nulo
            if (!is_null($simulado)) {
                $simulado->Titulo = Input::put('titulo');
                $simulado->Versao = Input::put('versao');
                // a data do cadastro não deve ser alterada
                //$simulado->Cadastro = $this->formatarDataSQL(Input::put('cadastro'));
                $simulado->Pontuacao = Input::put('pontuacao');
                $simulado->Duracao = Input::put('duracao');
                $simulado->Questoes = Input::put('questoes');
                $simulado->Inicio = $this->formatarDataSQL(Input::put('inicio'));
                $simulado->Fim = $this->formatarDataSQL(Input::put('fim'));
                $simulado->Exibirdescricaoinicio = (Input::put('exibirDescricaoInicio') === 'true') ? 'S' : 'N';
                $simulado->Descricao = Input::put('descricao');
                // tem que estar liberado
                //$simulado->Situacao = 'L'; // Liberado
                $simulado->save();

                echo 'ok';
            } else {
                echo 'nok';
            }
        } else {
            echo 'nok';
        }
    }

    /**
     * Remove o registro com a identificação fornecida.
     * @return array
     */
    public function delete_index()
    {
        $id = Input::delete('id', -1);
        $removido = false;

        // em tese o registro é válido
        if ($id > 0) {
            // procura o registro por sua identificação
            $simulado = Model_Simulado::find($id);

            // de fato o registro é válido pois não é nulo
            if (!is_null($simulado)) {
                // remove o registro/objeto
                $simulado->delete();

                // em tese o registro foi removido, vamos procurar por ele?
                $simulado = Model_Simulado::find($id);
                // o registro não foi encontrado (!!!)
                if (is_null($simulado)) {
                    // é informado que ele foi removido (!!!)
                    $removido = true;
                }
            }
        }

        // o registro não foi removido ou não pode ser removido
        if (!$removido) {
            // não existe a saída em json do método DELETE
            echo 'nok';
        } // o registro foi removido
        else {
            // não existe a saída em json do método DELETE
            echo 'ok';
        }
    }

    /**
     * Libera o registro com a identificação fornecida.
     * @return array
     */
    public function get_liberar()
    {
        // em tese o registro é válido
        $id = Uri::segment(5);
        $liberar = (Uri::segment(6) === 'sim');

        // primeiro verifica se em tese é uma identificação
        if ($id > 0) {
            // procura o registro por sua identificação
            $simulado = Model_Simulado::find($id);

            // de fato o registro é válido pois não é nulo
            if (!is_null($simulado)) {
                // verifica se sua situação não está finalizada
                if ($simulado->Situacao === 'F') {
                    echo 'nok';
                } // do contrário
                else {
                    // atualiza sua situação para edição ou execução
                    $simulado->Situacao = $liberar ? 'E' : 'X';
                    // salva o registro
                    $simulado->save();

                    echo 'ok';
                }
            }
        } else {
            echo 'nok';
        }
    }

    /**
     * Processa os registros obtidos.
     * @return array
     */
    private function processarRegistros($registros)
    {
        $retorno = array();

        foreach ($registros as $chave => $registro) {
            $rotulo = '<a href="javascript:void(0);" onclick="javascript:remover();" title="Remover \'' . $registro->Titulo . '\'"><i class="fam-delete" style="float:right;"></i></a>';
            switch ($registro->Situacao) {
                case 'X':
                    $rotulo = '<a href="javascript:void(0);" onclick="javascript:liberar(true);" title="Permitir edição"><i class="situacao fam-lock"></i></a> ';
                    break;

                case 'E':
                    $rotulo .= '<a href="javascript:void(0);" onclick="javascript:liberar(false);" title="Permitir execução"><i class="situacao fam-lock-open"></i></a> ';
                    break;

                case 'F':
                    $rotulo = '<i class="situacao fam-stop" title="Finalizado"></i> ';
                    break;
            }

            $rotulo .= $registro->Titulo;
            $valor = $registro->to_array();

            // formata os campos de data para o formato brasileiro
            $valor['Cadastro'] = $this->formatarDataExibicao($valor['Cadastro']);
            $valor['Inicio'] = $this->formatarDataExibicao($valor['Inicio']);
            $valor['Fim'] = $this->formatarDataExibicao($valor['Fim']);

            $retorno[] = array(
                'id' => $registro->id,
                'rotulo' => $rotulo,
                'valor' => $valor
            );

            // liberação da memória
            unset($registros[$chave]);
        }

        // liberação da memória
        unset($registros);

        // retorno do processamento dos registros
        return $retorno;
    }

    /**
     * Formata o objeto data/hora para a exibição.
     * @param $data Data/Hora para ser formatado
     * @return string
     */
    private function formatarDataExibicao($data)
    {
        if (!empty($data)) {
            $data = new DateTime($data);
            $data = $data->format('d/m/Y H:i');
        }

        return $data;
    }

    /**
     * Formata o texto data/hora para a SQL.
     * @param $data Texto data/Hora para ser formatado
     * @return string
     */
    private function formatarDataSQL($data)
    {
        if (!empty($data)) {
            // pt-BR
            $datetime = $data;
            $datetime = explode(' ', $datetime);
            $date = join('-', array_reverse(explode('/', $datetime[0])));
            $data = $date . ' ' . $datetime[1];
        }

        return $data;
    }

    /**
     * Vincula os assuntos.
     */
    function post_assuntos()
    {
        $id = Input::post('id');
        $assuntos = explode(',', Input::post('assuntos'));

        // remove todas as vinculações que houverem do simulado com as turmas, possibilitando que seja deselecionado
        // a turma
        $query = DB::query('DELETE FROM SimuladoAssunto WHERE (Simulado = ' . $id . ')');
        $query->execute();
        unset($query);

        // para cada assunto
        foreach ($assuntos as $key => $value) {
            if (strlen($value) > 0) {
                $value = explode('|', $value);

                // caso seja um assunto (na interface as disciplinas estão com valor "-1")
                if ($value[0] > 0) {
                    // verifica se já está cadastrado
                    $assunto = Model_Assunto::find($value[0]);

                    // caso não tenha sido encontrado
                    if (count($assunto) === 0) {
                        // remove o "<sup>Assunto</sup>"
                        $value[1] = explode('<', $value[1]);
                        $value[1] = $value[1][0];

                        // adiciona o assunto
                        $assunto = Model_Assunto::forge();
                        $assunto->id = $value[0];
                        $assunto->Nome = $value[1];
                        $assunto->save();
                    }

                    $assunto = $value[0];

                    // vincula o assunto com o simulado
                    $simuladoAssunto = Model_SimuladoAssunto::forge();
                    $simuladoAssunto->Simulado = $id;
                    $simuladoAssunto->Assunto = $assunto;
                    $simuladoAssunto->save();
                }
            }
        }

        echo 'ok';
    }

    /**
     * Vincula as turmas.
     */
    function post_turmas()
    {
        $id = Input::post('id');
        $turmas = explode(',', Input::post('turmas'));

        // remove todas as vinculações que houverem do simulado com as turmas, possibilitando que seja deselecionado
        // a turma
        $query = DB::query('DELETE FROM SimuladoTurma WHERE (Simulado = ' . $id . ')');
        $query->execute();
        unset($query);

        // para cada turma
        foreach ($turmas as $key => $value) {
            if (strlen($value) > 0) {
                $value = explode('|', $value);

                // verifica se já está cadastrada
                $turma = Model_Turma::find($value[0]);

                // caso não tenha sido encontrada
                if (count($turma) === 0) {
                    // adiciona a turma
                    $turma = Model_Turma::forge();
                    $turma->id = $value[0];
                    $turma->Nome = $value[1];
                    $turma->save();
                }

                $turma = $value[0];

                // vincula a turma com o simulado
                $simuladoTurma = Model_SimuladoTurma::forge();
                $simuladoTurma->Simulado = $id;
                $simuladoTurma->Turma = $turma;
                $simuladoTurma->save();
            }
        }

        echo 'ok';
    }

    /**
     * Vincula as questões.
     */
    function post_questoes()
    {
        $id = Input::post('id');
        $questoes = explode(',', Input::post('questoes'));

        // remove todas as vinculações que houverem do simulado com as questões, possibilitando que seja deselecionado
        // a questão
        $query = DB::query('DELETE FROM SimuladoQuestao WHERE (Simulado = ' . $id . ')');
        $query->execute();
        unset($query);

        // para cada questão
        foreach ($questoes as $key => $value) {
            if (strlen($value) > 0) {
                // obtém a questão
                $questao = Model_Questao::find($value);

                // caso tenha sido encontrada
                if (count($questao) > 0) {
                    // vincula a questão com o simulado
                    $simuladoQuestao = Model_SimuladoQuestao::forge();
                    $simuladoQuestao->Simulado = $id;
                    $simuladoQuestao->Assunto = $questao->Assunto;
                    $assunto = Model_Assunto::find($questao->Assunto);
                    $simuladoQuestao->AssuntoNome = $assunto->Nome;
                    $simuladoQuestao->Questao = $questao->id;
                    $simuladoQuestao->Tipo = $questao->Tipo;
                    $simuladoQuestao->Conteudo = $questao->Conteudo;
                    $simuladoQuestao->Explicacao = $questao->Explicacao;

                    $simuladoQuestao->save();

                    // obtém todas as respostas para vincular à questão do simulado
                    $simuladoQuestaoRespostas = Model_QuestaoResposta::find('all', array(
                        'where' => array(
                            array('Questao', $questao->id),
                        ),
                    ));

                    foreach ($simuladoQuestaoRespostas as $chave => $resposta) {
                        $simuladoQuestaoResposta = Model_SimuladoQuestaoResposta::forge();
                        $simuladoQuestaoResposta->SimuladoQuestao = $simuladoQuestao->id;
                        $simuladoQuestaoResposta->QuestaoResposta = $resposta->id;
                        $simuladoQuestaoResposta->Letra = $resposta->Letra;
                        $simuladoQuestaoResposta->Selecionada = $resposta->Selecionada;
                        $simuladoQuestaoResposta->Conteudo = $resposta->Conteudo;
                        $simuladoQuestaoResposta->save();
                    }
                }
            }
        }

        echo 'ok';
    }

    function get_relatorio()
    {
        $retorno = array(
            'aaData' => array()
        );

        $query = DB::query('
        SELECT
          SHA1(s.id) AS id,
          s.Titulo,
          ROUND(IFNULL(SUM(als.Duracao)/COUNT(als.id), 0), 2) AS TempoMedio,
          ROUND(IFNULL(SUM((als.Pontuacao * 100)/(SELECT si.Pontuacao FROM Simulado si WHERE (si.id = als.Simulado))), 0), 2) AS DesempenhoMedio
        FROM
          Simulado s
          LEFT JOIN AlunoSimulado als ON
            (als.Simulado = s.id)');
        $result = $query->execute();

        while (!is_null($result->current())) {
            $retorno['aaData'][] = (object)$result->current();

            $result->next();
        }

        return $retorno;
    }
}