<?php

/**
 * Class Controller_Rest_Cadastro_Assuntos
 *
 * Classe que cuida do CRUD do cadastro dos assuntos.
 */
class Controller_Rest_Cadastro_Assuntos extends Controller_Rest
{

    private $usuario;

    /**
     * Construtor da classe.
     */
    function __construct(\Request $request) // nesse caso o construtor recebe a requisição tal qual o construtor de seu pai
    {
        // chama o construtor da classe pai
        parent::__construct($request);

        // obtém o usuário da sessão
        $usuario = \Session::get('usuario');

        // obtém a pessoa
        $pessoas = Model_Pessoa::find('all', array(
            'where' => array(
                array('usuario', $usuario->usuario),
            ),
        ));

        foreach ($pessoas as $value) {
            $this->usuario = $value;
        }
    }

    /**
     * Obtém todos os registros para a listagem do seu cadastro.
     * @return array
     */
    public function get_index()
    {
        // obtém o parâmetro de pesquisa
        $procurarpor = Input::get('procurarpor');

        $onde = null;
        switch ($this->usuario->Nivel) {
            case 'Professor':
                if (!empty($procurarpor)) {
                    $onde = array(
                        'CriadoPor' => $this->usuario->id,
                        'and' => array('Nome', 'LIKE', '%' . $procurarpor . '%'),
                    );
                } else {
                    $onde = array(
                        'CriadoPor' => $this->usuario->id
                    );
                }
                break;
            case 'Coordenador':
                if (!empty($procurarpor)) {
                    $onde = array('Nome', 'LIKE', '%' . $procurarpor . '%');
                }
                break;
        }

        // obtém todos os registros
        if (!is_null($onde)) {
            $registros = Model_Assunto::find('all', array(
                    'where' => $onde,
                    'order_by' => array('Nome' => 'asc'))
            );
        } else {
            $registros = Model_Assunto::find('all', array(
                    'order_by' => array('Nome' => 'asc'))
            );
        }

        // processamento dos registros, pois eles precisam ser formatados para aproveitar a estrutura do jqxListBox
        $registros = $this->processarRegistros($registros);

        // retorna os registros encontrados e processados
        return $registros;
    }

    /**
     * Insere um novo registro.
     * @return array
     */
    public function post_index()
    {
        // verifica se o assunto já está cadastrado e é da mesma disciplina
        $assuntos = Model_Assunto::find('all', array(
            'where' => array(
                'nome' => Input::post('nome'),
                'and' => array(
                    'disciplina', Input::post('disciplina')
                ),
            ),
        ));

        // caso tenha sido encontrado
        if (count($assuntos) > 0) {
            echo 'Já existe um assunto com este nome e esta disciplina.';
        } else {
            $this->processarDisciplina(Input::post('disciplina'), Input::post('disciplinaNome'));

            $assunto = Model_Assunto::forge();
            $assunto->Nome = Input::post('nome');
            $assunto->Disciplina = Input::post('disciplina');
            $assunto->CriadoPor = $this->usuario->id;
            $assunto->save();

            echo 'ok';
        }
    }

    /**
     * Modifica um registro com a identificação fornecida.
     */
    public function put_index()
    {
        $id = Input::put('id');

        // em tese o registro é válido
        if ($id > 0) {
            // procura o registro por sua identificação
            $assunto = Model_Assunto::find($id);

            // de fato o registro é válido pois não é nulo
            if (!is_null($assunto)) {
                $this->processarDisciplina(Input::put('disciplina'), Input::put('disciplinaNome'));

                $assunto->Nome = Input::put('nome');
                $assunto->Disciplina = Input::put('disciplina');
                $assunto->save();

                echo 'ok';
            } else {
                echo 'nok';
            }
        } else {
            echo 'nok';
        }

    }

    /**
     * Remove o registro com a identificação fornecida.
     * @return array
     */
    public function delete_index()
    {
        $id = Input::delete('id', -1);
        $removido = false;

        // em tese o registro é válido
        if ($id > 0) {
            // procura o registro por sua identificação
            $assunto = Model_Assunto::find($id);

            // de fato o registro é válido pois não é nulo
            if (!is_null($assunto)) {
                // remove o registro/objeto
                $assunto->delete();

                // em tese o registro foi removido, vamos procurar por ele?
                $assunto = Model_Assunto::find($id);
                // o registro não foi encontrado (!!!)
                if (is_null($assunto)) {
                    // é informado que ele foi removido (!!!)
                    $removido = true;
                }
            }
        }

        // o registro não foi removido ou não pode ser removido
        if (!$removido) {
            // não existe a saída em json do método DELETE
            echo 'nok';
        } // o registro foi removido
        else {
            // não existe a saída em json do método DELETE
            echo 'ok';
        }
    }

    /**
     * Processa os registros obtidos.
     * @return array
     */
    private function processarRegistros($registros)
    {
        $retorno = array();

        foreach ($registros as $chave => $registro) {
            $rotulo = '<a href="javascript:void(0);" onclick="javascript:remover();" title="Remover \'' . $registro->Nome . '\'"><i class="fam-delete" style="float:right;"></i></a>';

            $rotulo .= $registro->Nome;
            $valor = $registro->to_array();

            $retorno[] = array(
                'id' => $registro->id,
                'rotulo' => $rotulo,
                'valor' => $valor
            );

            // liberação da memória
            unset($registros[$chave]);
        }

        // liberação da memória
        unset($registros);

        // retorno do processamento dos registros
        return $retorno;
    }

    /**
     * Verifica e adiciona a disciplina, caso não exista.
     *
     * @param $id Identificação da disciplina
     * @param $nome Nome da disciplina
     */
    private function processarDisciplina($id, $nome)
    {
        // verifica se a disciplina já existe
        $disciplinas = Model_Disciplina::find('all', array(
            'where' => array(
                array('id', $id),
            ),
        ));

        // caso a disciplina não exista
        if (count($disciplinas) === 0) {
            // cadastra a disciplina
            $disciplina = Model_Disciplina::forge();
            $disciplina->id = $id;
            $disciplina->Nome = $nome;
            $disciplina->save();
        }

        // verifica se existe um relacionamento do professor com a disciplina
        $professor = $this->usuario->id;
        $professorDisciplina = Model_ProfessorDisciplina::find('all', array(
            'where' => array(
                array('Professor', $professor),
                'and' => array('Disciplina', $id),
            ),
        ));

        // caso não haja relacionamento
        if (count($professorDisciplina) === 0) {
            // relaciona o professor com a disciplina
            $professorDisciplina = Model_ProfessorDisciplina::forge();
            $professorDisciplina->Professor = $professor;
            $professorDisciplina->Disciplina = $id;
            $professorDisciplina->save();
        }
    }

    /**
     * Obtém uma lista de todos os registros de disciplinas.
     * @return array
     */
    public function get_disciplinas()
    {
        $retorno = array();

        // obtém as disciplinas
        $query = DB::query('
        SELECT
          d.id,
          d.Nome
        FROM
          Assunto a
          INNER JOIN Disciplina d ON
            (a.Disciplina = d.id)
        GROUP BY
          d.id,
          d.Nome
        ORDER BY
          d.Nome ASC
        ');

        $result = $query->execute();

        while (!is_null($result->current())) {
            $record = (object)$result->current();

            $retorno[] = (object)array(
                'label' => $record->Nome . ' <sup>Disciplina</sup>',
                'expanded' => true,
                'value' => (int)$record->id,
                'items' => array(),
            );

            $result->next();
        }

        // para cada disciplina
        foreach ($retorno as $key => $value) {
            // obtém os assuntos
            $query = DB::query('
            SELECT
              a.id,
              a.Nome
            FROM
              Assunto a
            WHERE
              (a.Disciplina = ' . $value->value . ')
            ORDER BY
              a.Nome ASC');

            $result = $query->execute();

            while (!is_null($result->current())) {
                $record = (object)$result->current();

                $retorno[$key]->value = -1;
                $retorno[$key]->items[] = (object)array(
                    'label' => $record->Nome . ' <sup>Assunto</sup>',
                    'value' => (int)$record->id,
                );

                $result->next();
            }
        }

        return $retorno;
    }

    /**
     * Obtém uma lista de todos os registros de simulados.
     * @return array
     */
    public function post_simulado()
    {
        $retorno = array();

        if (($this->usuario->Nivel === 'Professor') || ($this->usuario->Nivel === 'Coordenador')) {

            $id = Input::post('id');

            $vinculados = Model_SimuladoAssunto::find('all', array(
                'where' => array(
                    array('Simulado', $id),
                ),
            ));

            foreach ($vinculados as $vinculado) {
                $retorno[] = $vinculado->Assunto;
            }
        }

        // retorna os registros encontrados e processados
        return $retorno;
    }

    /**
     * Obtém uma lista de todos os registros dos assuntos por simulado (para janela de vinculação dos assuntos).
     * @return array
     */
    public function get_assuntos_simulado()
    {
        $retorno = array();

        // obtém o simulado
        $simulado = Uri::segment(6);

        // obtém as disciplinas
        $query = DB::query('
        SELECT
          d.id,
          d.Nome
        FROM
          SimuladoAssunto sa
          INNER JOIN Assunto a ON
            (a.id = sa.Assunto)
          INNER JOIN Disciplina d ON
            (a.Disciplina = d.id)
        WHERE
          (sa.Simulado = ' . $simulado . ')
        GROUP BY
          d.id,
          d.Nome
        ORDER BY
          d.Nome ASC');

        $result = $query->execute();

        while (!is_null($result->current())) {
            $record = (object)$result->current();

            $retorno[] = (object)array(
                'label' => $record->Nome . ' <sup>Disciplina</sup>',
                'expanded' => true,
                'value' => (int)$record->id,
                'items' => array(),
            );

            $result->next();
        }

        // para cada disciplina
        foreach ($retorno as $key => $value) {
            // obtém os assuntos
            $query = DB::query('
            SELECT
              a.id,
              a.Nome
            FROM
              SimuladoAssunto sa
            INNER JOIN Assunto a ON
              (a.id = sa.Assunto)
            WHERE
              (sa.Simulado = ' . $simulado . ') AND
              (a.Disciplina = ' . $value->value . ')
            ORDER BY
              a.Nome ASC');

            $result = $query->execute();

            while (!is_null($result->current())) {
                $record = (object)$result->current();

                $retorno[$key]->value = -1;
                $retorno[$key]->items[] = (object)array(
                    'label' => $record->Nome . ' <sup>Assunto</sup>',
                    'value' => (int)$record->id,
                );

                $result->next();
            }
        }

        return $retorno;
    }

    /**
     * Obtém uma lista de todos os questãos do assunto (para janela de vinculação das questões).
     *
     * @return array
     */
    public function get_questoes()
    {
        $retorno = array();

        // obtém o simulado
        $assunto = Uri::segment(5);

        // obtém as disciplinas
        $query = DB::query('SELECT id, CONCAT(id, " - ", SUBSTRING(StripTags(TRIM(Conteudo)), 1, 50)) AS Nome FROM Questao WHERE (Assunto = ' . $assunto . ')');

        $result = $query->execute();

        while (!is_null($result->current())) {
            $record = (object)$result->current();

            $retorno[] = (object)array(
                'rotulo' => $record->Nome,
                'valor' => (int)$record->id,
            );

            $result->next();
        }

        return $retorno;
    }
}