<style>
    #assuntos-dialog-area ul {
        margin-left: 0;
    }

    #assuntos-dialog-area sup {
        font-size: 0.6em;
        color: #888;
    }

    #assuntos-dialog-area .jqx-fill-state-hover sup, #assuntos-dialog-area .jqx-fill-state-pressed sup {
        color: #fff;
    }
</style>
<script type="text/javascript">
var endereco, salvado;

endereco = '<?php Uri::base(); ?>/sisim/rest/cadastro/simulados';
salvado = false;

/**
 * Inicialização do processamento.
 */
$(document).ready(function () {
    var origem, adaptadorDeDados;

    // habilita a caixa de procura
    $('#procurar').enterKey(function () {
        // atualiza a listagem
        $('#simulados').jqxListBox('refresh');
    });

    // prepara os dados
    origem =
    {
        datatype: 'json',
        datafields: [
            {
                name: 'rotulo'
            },
            {
                name: 'valor'
            }
        ],
        id: 'id',
        url: endereco + '.json'
    };
    adaptadorDeDados = new $.jqx.dataAdapter(origem, {
        loadServerData: function (serverdata, source, callback) {
            var procurarpor;

            procurarpor = $('#procurar').val().trim();

            if (procurarpor.length > 0) {
                serverdata = {
                    'procurarpor': procurarpor
                };
            }

            $.ajax({
                dataType: source.datatype,
                url: source.url,
                data: serverdata,
                success: function (data, status, xhr) {
                    var dataArray, i, record, datarow, datafield, value;

                    dataArray = new Array();

                    if (data !== undefined) {
                        for (i = 0; i < data.length; (i = i + 1)) {
                            record = data[i];
                            datarow = {};

                            datarow['rotulo'] = record.rotulo;
                            datarow['valor'] = record.valor;

                            dataArray[dataArray.length] = datarow;
                        }
                    }

                    // envia os registros carregados para o plugin jqxDataAdapter
                    callback({ records: dataArray });

                    setTimeout('selecionarAoAdicionar()', 50);
                    setTimeout('destacarProcuraNaListagem("procurar", "simulados")', 50);
                }
            });
        }
    });
    // cria um jqxListBox
    $('#simulados').jqxListBox({
        source: adaptadorDeDados,
        displayMember: 'rotulo',
        valueMember: 'valor',
        width: '100%',
        height: 303,
        theme: 'bootstrap'
    });
    $('#simulados').on('select', function (event) {
        var item, value;

        if (event.args) {
            item = event.args.item;
            if (item) {
                value = item.value;

                $('#id').val(value.id);
                $('#titulo').val(value.Titulo);
                $('#versao').val(value.Versao);
                $('#cadastro').val(value.Cadastro).parent().show();
                $('#pontuacao').val(value.Pontuacao);
                $('#duracao').val(value.Duracao);
                $('#inicio').val(value.Inicio);
                $('#fim').val(value.Fim);
                $('#exibirDescricaoInicio').attr('checked', (value.ExibirDescricaoInicio === 'S'));
                $('#descricao').val(value.Descricao);

                liberarFormulario(value.Situacao === 'E');

                // DONE: Correção para a validação que ocorria quando o formulário ficava vazio e o campo de título
                // perdia o foco
                $('#titulo').blur().focus();

                $('#painel-controle .esquerda').fadeIn('fast');

                removerNotificacoesErro();
            }
        }
    });

    // prepara os campos
    $('#titulo')
        .filter_input({
            regex: '[A-Za-zÀ-ú ]'
        });
    $('#versao, #pontuacao, #duracao')
        .filter_input({
            regex: '[0-9]'
        })
        .spinner({
            min: 0
        });
    // DONE: Corrige a aparência dos "spinners"
    $('.ui-spinner-input').removeClass('ui-spinner-input');
    $('#inicio, #fim').datetimepicker();

    // adiciona um asterisco vermelho para os campos obrigatórios
    definirComoObrigatorio();

    // define a validação do formulário
    $('#simulado').validate({
        rules: {
            'titulo': 'required',
            'versao': 'required',
            'pontuacao': 'required',
            'duracao': 'required',
            'inicio': 'required',
            'fim': {
                'greaterThan': ['#inicio', 'Data e Hora de Início']
            }
        },
        onfocusout: function (element) {
            this.element(element);
        },
        onkeyup: false,
        errorPlacement: function (error, element) {
            var existe, mensagem, mensagens, i;

            existe = false;
            mensagem = '<i class="icon-warning-sign"></i> ' + element.data('titulo') + ': ' + error.text();

            // verifica se já existe uma notificação com o título
            mensagens = $('.bootstrap-growl').toArray();
            for (i = 0; i < mensagens.length; (i = i + 1)) {
                if ($(mensagens[i]).html() === mensagem) {
                    existe = true;
                    break;
                }
            }

            if (!existe) {
                notificarErro(mensagem);
            }
        },
        submitHandler: function (form) {
            var id, titulo, versao, pontuacao, duracao, inicio, fim, exibirDescricaoInicio, descricao;

            // remove todas as notificações
            $('.qtip').remove();

            // obtém os valores dos campos
            id = $('#id').val();
            titulo = $('#titulo').val();
            versao = $('#versao').val();
            pontuacao = $('#pontuacao').val();
            duracao = $('#duracao').val();
            inicio = $('#inicio').val();
            fim = $('#fim').val();
            exibirDescricaoInicio = $('#exibirDescricaoInicio').prop('checked');
            descricao = $('#descricao').val();

            // notifica ao formulário que ele está/foi salvo
            salvado = true;

            // solicita a inserção/alteração do registro
            notificarInformacao('<i class="icon-info-sign"></i> Salvando...');
            $.ajax({
                url: endereco,
                data: {
                    'id': id,
                    'titulo': titulo,
                    'versao': versao,
                    'pontuacao': pontuacao,
                    'duracao': duracao,
                    'inicio': inicio,
                    'fim': fim,
                    'exibirDescricaoInicio': exibirDescricaoInicio,
                    'descricao': descricao
                },
                type: (id.length === 0) ? 'POST' : 'PUT',
                success: function (result) {

                    removerNotificacoes();

                    // se tiver sido OK
                    if (result === 'ok') {
                        notificarSucesso('<i class="icon-ok"></i> Salvo com sucesso!');

                        // atualiza a lista de simulados
                        $('#simulados').jqxListBox('refresh');
                    }
                    // do contrário
                    else {
                        notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao salvar.');
                    }
                },
                error: function (result) {
                    notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao salvar.');
                }
            });
        }
    });

    // ao clicar no botão "Adicionar"
    $('#adicionar').click(function () {
        removerNotificacoes();

        // deseleciona algum item da listagem
        $('#simulados').jqxListBox('clearSelection');

        // limpa o formulário
        $('#id, #titulo, #cadastro, #inicio, #fim, #descricao').val(new String());
        $('#cadastro').parent().hide()
        $('#versao, #pontuacao, #duracao').val(0);
        $('#exibirDescricaoInicio').attr('checked', false);

        $('#painel-controle .esquerda').hide();

        liberarFormulario(true);
    }).click();

    // ao clicar no botão "Salvar"
    $('#salvar').click(function (e) {
        var janela;

        e.preventDefault();

        // previne continuar a ação caso o botão esteja desabilitado
        if ($(this).hasClass('disabled')) {
            return false;
        }

        janela = $('<div id="#dialog-salvar" />')
            .html('Deseja salvar?')
            .dialog({
                modal: true,
                closeOnEscape: false,
                resizable: false,
                title: 'Confirmação',
                buttons: {
                    'Não': function () {
                        $(this).dialog('close');

                        return false;
                    },
                    'Sim': function () {
                        var form;

                        $(this).dialog('close');

                        // valida os campos do formulário e caso seja válido, envia
                        form = $('#simulado');
                        if (form.valid()) {
                            form.submit();
                        }

                        return false;
                    }
                },
                open: function () {
                    // remove o botão de fechar
                    $(this).parent().find('.ui-dialog-titlebar-close').remove();

                    $('.ui-dialog-buttonpane button', $(this).parent()).each(function () {
                        $(this).addClass('btn');
                        if ($(this).text() === 'Sim') {
                            $(this).prepend('<i class="fam-tick"></i> ');
                        } else if ($(this).text() === 'Não') {
                            $(this).prepend('<i class="fam-cross"></i> ').focus();
                        }
                    });
                    //addClass('btn').focus();
                },
                close: function () {
                    // destrói a janela
                    $(janela).parent().remove();
                    $(janela).remove();
                    janela = null;
                }
            });
    });


    // ao clicar no botão "Turmas"
    $('#turmas').click(function (e) {
        // evita o formulário ser enviado, sendo que queremos adicionar outras funcionalidades
        e.preventDefault();

        $('#turmas-dialog').remove();

        $(document.body).append('<div id="turmas-dialog">\n\
            <div><span>Vinculação de Turmas</span></div>\n\
            <div style="overflow:hidden;">\n\
                <div id="turmas-dialog-loader"><?php echo Asset::img('loader.gif'); ?> Carregando...</div>\n\
                <div id="turmas-dialog-area"></div>\n\
                <div id="turmas-dialog-controle" style="float: right; margin-top: 5px;">\n\
                    <input type="button" id="turmas-dialog-cancelar" value="Cancelar" style="margin-right: 2px" />\n\
                    <input type="button" id="turmas-dialog-ok" value="OK" />\n\
                </div>\n\
            </div>\n\
        </div>');

        $('#turmas-dialog').jqxWindow({
            showCollapseButton: false,
            maxHeight: 400,
            maxWidth: 200,
            minHeight: 200,
            minWidth: 200,
            height: 300,
            width: 100,
            theme: 'bootstrap',
            isModal: true,
            modalOpacity: 0.3,
            resizable: false,
            okButton: $('#turmas-dialog-ok'),
            cancelButton: $('#turmas-dialog-cancelar'),
            initContent: function () {
                var item;

                item = $('#simulados').jqxListBox('getSelectedItem');

                $('#turmas').jqxWindow('focus');

                $('#turmas-dialog-controle').hide();
                $('#turmas-dialog-ok')
                    .jqxButton({ theme: 'bootstrap', width: '65px' });

                // não está em modo de edição
                if (item.value.Situacao !== 'E') {
                    $('#turmas-dialog-ok')
                        .jqxButton({ disabled: true });
                }

                $('#turmas-dialog-cancelar')
                    .jqxButton({ theme: 'bootstrap', width: '75px' });

                // prepara os daos
                var source =
                {
                    datatype: 'json',
                    datafields: [
                        { name: 'rotulo' },
                        { name: 'valor' }
                    ],
                    id: 'id',
                    url: endereco + '/../turmas.json'
                };
                var dataAdapter = new $.jqx.dataAdapter(source, {
                    downloadComplete: function (edata, textStatus, jqXHR) {
                        /**
                         * Carrega as turmas vinculadas ao simulado.
                         */
                        $.post(endereco + '/../turmas/simulado.json', {
                            'id': $('#id').val()
                        }, function (turmas) {

                            // para cada turma, marca a sua seleção
                            $(turmas).each(function () {
                                var turma, lista, items;

                                turma = this;
                                lista = $('#turmas-dialog-area');
                                items = lista.jqxListBox('getItems');

                                $(items).each(function () {
                                    if (this.value == turma) {
                                        lista.jqxListBox('checkItem', this);
                                    }
                                });
                            });

                            $('#turmas-dialog-loader').fadeOut('fast', function () {
                                $('#turmas-dialog-area, #turmas-dialog-controle').fadeIn('fast', function () {
                                    $('#turmas-dialog-cancelar').focus();
                                });
                            })
                        });
                    }
                });

                // cria a caixa de listagem
                $('#turmas-dialog-area').jqxListBox({
                    source: dataAdapter,
                    displayMember: 'rotulo',
                    valueMember: 'valor',
                    width: 185,
                    height: 230,
                    theme: 'bootstrap',
                    checkboxes: true
                }).hide();
            }
        })
            .on('close', function (event) {
                if (event.args.dialogResult.OK) {
                    var items, turmas;

                    items = $('#turmas-dialog-area').jqxListBox('getCheckedItems');
                    turmas = '';
                    $(items).each(function () {
                        turmas += this.value + '|' + this.label + ',';
                    });

                    // solicita a vinculação dos items
                    notificarInformacao('<i class="icon-info-sign"></i> Vinculando turmas...');
                    $.ajax({
                        url: endereco + '/turmas',
                        data: {
                            'id': $('#id').val(),
                            'turmas': turmas
                        },
                        type: 'POST',
                        success: function (result) {

                            removerNotificacoes();

                            // se tiver sido OK
                            if (result === 'ok') {
                                notificarSucesso('<i class="icon-ok"></i> Vinculação efetuada com sucesso!');
                            }
                            // do contrário
                            else {
                                notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao vincular.');
                            }
                        },
                        error: function (result) {
                            notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao vincular.');
                        }
                    });
                }
            });
    });

    // ao clicar no botão "Assuntos"
    $('#assuntos').click(function (e) {
        // evita o formulário ser enviado, sendo que queremos adicionar outras funcionalidades
        e.preventDefault();

        $('#assuntos-dialog').remove();

        $(document.body).append('<div id="assuntos-dialog">\n\
            <div><span>Vinculação de Assuntos</span></div>\n\
            <div style="overflow:hidden;">\n\
                <div id="assuntos-dialog-loader"><?php echo Asset::img('loader.gif'); ?> Carregando...</div>\n\
                <div id="assuntos-dialog-area"></div>\n\
                <div id="assuntos-dialog-controle" style="float: right; margin-top: 5px;">\n\
                    <input type="button" id="assuntos-dialog-cancelar" value="Cancelar" style="margin-right: 2px" />\n\
                    <input type="button" id="assuntos-dialog-ok" value="OK" />\n\
                </div>\n\
            </div>\n\
        </div>');

        $('#assuntos-dialog').jqxWindow({
            showCollapseButton: false,
            maxHeight: 400,
            maxWidth: 550,
            minHeight: 200,
            minWidth: 200,
            height: 300,
            width: 450,
            theme: 'bootstrap',
            isModal: true,
            modalOpacity: 0.3,
            resizable: false,
            okButton: $('#assuntos-dialog-ok'),
            cancelButton: $('#assuntos-dialog-cancelar'),
            initContent: function () {
                var item, tree, source;

                item = $('#simulados').jqxListBox('getSelectedItem');
                tree = $('#assuntos-dialog-area');
                source = null;

                $('#assuntos').jqxWindow('focus');

                $('#assuntos-dialog-controle').hide();
                $('#assuntos-dialog-ok')
                    .jqxButton({ theme: 'bootstrap', width: '65px' });

                // não está em modo de edição
                if (item.value.Situacao !== 'E') {
                    $('#assuntos-dialog-ok')
                        .jqxButton({ disabled: true });
                }

                $('#assuntos-dialog-cancelar')
                    .jqxButton({ theme: 'bootstrap', width: '75px' });

                $.ajax({
                    async: false,
                    dataType: 'json',
                    url: endereco + '/../assuntos/disciplinas.json',
                    success: function (data, status, xhr) {
                        source = data;
                    }
                });

                $('#assuntos-dialog-area').jqxTree({
                    source: source,
                    width: 437,
                    height: 230,
                    theme: 'bootstrap',
                    checkboxes: true,
                    hasThreeStates: true
                }).hide();

                /**
                 * Carrega as turmas vinculadas ao simulado.
                 */
                $.post(endereco + '/../assuntos/simulado.json', {
                    'id': $('#id').val()
                }, function (assuntos) {

                    // para cada assunto, marca a sua seleção
                    $(assuntos).each(function () {
                        var assunto, lista, items;

                        assunto = this;
                        lista = $('#assuntos-dialog-area');
                        items = lista.jqxTree('getItems');

                        $(items).each(function () {
                            if (this.value == assunto) {
                                lista.jqxTree('checkItem', this);
                            }
                        });
                    });

                    $('#assuntos-dialog-loader').fadeOut('fast', function () {
                        $('#assuntos-dialog-area, #assuntos-dialog-controle').fadeIn('fast', function () {
                            $('#assuntos-dialog-cancelar').focus();
                        });
                    });
                });
            }
        })
            .on('close', function (event) {
                if (event.args.dialogResult.OK) {
                    var items, assuntos;

                    items = $('#assuntos-dialog-area').jqxTree('getCheckedItems');
                    assuntos = '';
                    $(items).each(function () {
                        if (this.value > 0) {
                            assuntos += this.value + '|' + this.label + ',';
                        }
                    });

                    // solicita a vinculação dos items
                    notificarInformacao('<i class="icon-info-sign"></i> Vinculando assuntos...');
                    $.ajax({
                        url: endereco + '/assuntos',
                        data: {
                            'id': $('#id').val(),
                            'assuntos': assuntos
                        },
                        type: 'POST',
                        success: function (result) {

                            removerNotificacoes();

                            // se tiver sido OK
                            if (result === 'ok') {
                                notificarSucesso('<i class="icon-ok"></i> Vinculação efetuada com sucesso!');
                            }
                            // do contrário
                            else {
                                notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao vincular.');
                            }
                        },
                        error: function (result) {
                            notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao vincular.');
                        }
                    });
                }
            });
    });

    // ao clicar no botão "Questões"
    $('#questoes').click(function (e) {
        // evita o formulário ser enviado, sendo que queremos adicionar outras funcionalidades
        e.preventDefault();

        $('#questoes-dialog').remove();

        $(document.body).append('<div id="questoes-dialog">\n\
            <div><span>Vinculação de Questões</span></div>\n\
            <div style="overflow:hidden;">\n\
                <div id="questoes-dialog-loader"><?php echo Asset::img('loader.gif'); ?> Carregando...</div>\n\
                <div id="questoes-dialog-area" style="display:none;">\n\
                    <div class="form-inline two-width">\n\
                        <div id="questoes-assuntos-dropDownButton">\n\
                            <div style="border: none;" id="questoes-assuntos-Tree"></div>\n\
                        </div>\n\
                    </div>\n\
                    <div id="questoes-assuntos-questoes" />\n\
                </div>\n\
                <div id="questoes-dialog-controle" style="float: right; margin-top: 5px;">\n\
                    <input type="button" id="questoes-dialog-cancelar" value="Cancelar" style="margin-right: 2px" />\n\
                    <input type="button" id="questoes-dialog-ok" value="OK" />\n\
                </div>\n\
            </div>\n\
        </div>');

        $('#questoes-dialog').jqxWindow({
            showCollapseButton: false,
            maxHeight: 400,
            maxWidth: 550,
            minHeight: 200,
            minWidth: 200,
            height: 300,
            width: 450,
            theme: 'bootstrap',
            isModal: true,
            modalOpacity: 0.3,
            resizable: false,
            okButton: $('#questoes-dialog-ok'),
            cancelButton: $('#questoes-dialog-cancelar'),
            initContent: function () {
                var item, source, questoesAssuntosDropDownButton, questoesAssuntosTree, questoes;

                item = $('#simulados').jqxListBox('getSelectedItem');
                source = null;

                questoes = $('#questoes-assuntos-questoes');

                $('#questoes').jqxWindow('focus');

                $('#questoes-dialog-controle').hide();
                $('#questoes-dialog-ok')
                    .jqxButton({ theme: 'bootstrap', width: '65px' });

                // não está em modo de edição
                if (item.value.Situacao !== 'E') {
                    $('#questoes-dialog-ok')
                        .jqxButton({ disabled: true });
                }

                $('#questoes-dialog-cancelar')
                    .jqxButton({ theme: 'bootstrap', width: '75px' });

                // obtém as questões do simulado
                window.questoesVinculadas = [];

                $.ajax({
                    async: false,
                    dataType: 'json',
                    url: endereco + '/../questoes/simulado/' + $('#id').val(),
                    success: function (data, status, xhr) {
                        window.questoesVinculadas = data;
                    }
                });

                // popula os dados do combo Assunto
                questoesAssuntosDropDownButton = $('#questoes-assuntos-dropDownButton');
                questoesAssuntosTree = $('#questoes-assuntos-Tree');

                $.ajax({
                    async: false,
                    dataType: 'json',
                    url: endereco + '/../assuntos/questoes/simulado/' + $('#id').val(),
                    success: function (data, status, xhr) {
                        source = data;
                    }
                });

                questoesAssuntosDropDownButton
                    .jqxDropDownButton({
                        width: '100%',
                        height: 25,
                        theme: 'bootstrap'
                    });
                questoesAssuntosTree
                    .on('initialized', function (event) {
                        var questoesAssuntosTree, items;

                        questoesAssuntosTree = $('#questoes-assuntos-Tree');
                        items = questoesAssuntosTree.jqxTree('getItems');

                        $(items).each(function () {
                            if (this.label.indexOf('Disciplina') > 0) {
                                questoesAssuntosTree.jqxTree('disableItem', this.element);
                            }
                        });
                    })
                    .on('select', function (event) {
                        var args, item, dropDownContent, origem;

                        args = event.args;
                        item = questoesAssuntosTree.jqxTree('getItem', args.element);

                        if (item.label.indexOf('Disciplina') < 0) {
                            dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + item.label + '</div>';
                            questoesAssuntosDropDownButton.jqxDropDownButton('setContent', dropDownContent);

                            questoesAssuntosDropDownButton.jqxDropDownButton('close');

                            // prepara os dados
                            origem =
                            {
                                datatype: 'json',
                                datafields: [
                                    {
                                        name: 'rotulo'
                                    },
                                    {
                                        name: 'valor'
                                    }
                                ],
                                id: 'id',
                                url: endereco + '/../assuntos/questoes/' + item.value
                            };

                            adaptadorDeDados = new $.jqx.dataAdapter(origem, {
                                loadServerData: function (serverdata, source, callback) {
                                    var procurarpor;

                                    procurarpor = $('#procurar').val().trim();

                                    if (procurarpor.length > 0) {
                                        serverdata = {
                                            'procurarpor': procurarpor
                                        };
                                    }

                                    $.ajax({
                                        dataType: source.datatype,
                                        url: source.url,
                                        data: serverdata,
                                        success: function (data, status, xhr) {
                                            var dataArray, i, record, datarow, datafield, value;

                                            dataArray = new Array();

                                            if (data !== undefined) {
                                                for (i = 0; i < data.length; (i = i + 1)) {
                                                    record = data[i];
                                                    datarow = {};

                                                    datarow['rotulo'] = record.rotulo;
                                                    datarow['valor'] = record.valor;

                                                    dataArray[dataArray.length] = datarow;
                                                }
                                            }

                                            // envia os registros carregados para o plugin jqxDataAdapter
                                            callback({ records: dataArray });
                                        }
                                    });
                                }
                            });

                            questoes.jqxListBox({
                                source: adaptadorDeDados
                            });
                        }
                    })
                    .jqxTree({
                        source: source,
                        width: 440,
                        height: 220,
                        theme: 'bootstrap'
                    });

                // cria um jqxListBox
                questoes.jqxListBox({
                    source: null,
                    checkboxes: true,
                    displayMember: 'rotulo',
                    valueMember: 'valor',
                    width: '100%',
                    height: 200,
                    theme: 'bootstrap'
                })
                    .on('bindingComplete', function (event) {
                        var item, i;

                        // varre o vetor
                        for (i = 0; i < window.questoesVinculadas.length; (i = i + 1)) {
                            // obtém o item
                            item = questoes.jqxListBox('getItemByValue', window.questoesVinculadas[i]);

                            // caso encontre
                            if (item !== null) {
                                questoes.jqxListBox('checkItem', item);
                            }
                        }
                    })
                    .on('checkChange', function (event) {
                        var args, checked, item, itemLabel, itemValue, i, temp;

                        args = event.args;

                        // get new check state.
                        checked = args.checked;

                        // get the item and it's label and value fields.
                        item = args.item;
                        itemLabel = item.label;
                        itemValue = item.value;

                        // caso hajam elementos
                        if (window.questoesVinculadas.length > 0) {
                            temp = [];

                            for (i = 0; i < window.questoesVinculadas.length; (i = i + 1)) {
                                if (item.value !== window.questoesVinculadas[i]) {
                                    temp.push(window.questoesVinculadas[i]);
                                }
                            }

                            // se foi marcado
                            if (checked) {
                                temp.push(item.value);
                            }

                            window.questoesVinculadas = temp;
                        }
                        // caso não hajam elementos e tenha sido marcado
                        else if (checked) {
                            window.questoesVinculadas.push(itemValue);
                        }

                        // get all checked items.
                        //var checkedItems = $("#jqxListBox").jqxListBox('getCheckedItems');
                    });

                $('#questoes-dialog-loader').fadeOut('fast', function () {
                    $('#questoes-dialog-area, #questoes-dialog-controle').fadeIn('fast', function () {
                        $('#questoes-dialog-cancelar').focus();
                    });
                });
            }
        })
            .on('close', function (event) {
                var questoesAssuntosDropDownButton, questoesAssuntosTree;

                if (typeof event.args !== 'undefined') {
                    if (typeof event.args.dialogResult !== 'undefined') {
                        if ((event.args.dialogResult.Cancel) || (event.args.dialogResult.OK)) {
                            questoesAssuntosDropDownButton = $('#questoes-assuntos-dropDownButton');
                            questoesAssuntosTree = $('#questoes-assuntos-Tree');

                            // sanitiza
                            questoesAssuntosDropDownButton.jqxDropDownButton('destroy');
                            questoesAssuntosTree.jqxTree('destroy');
                        }

                        if (event.args.dialogResult.OK) {
                            var i, questoes;

                            questoes = '';
                            for (i = 0; i < window.questoesVinculadas.length; (i = i + 1)) {
                                questoes += window.questoesVinculadas[i] + ',';
                            }

                            // solicita a vinculação dos items
                            notificarInformacao('<i class="icon-info-sign"></i> Vinculando questões...');
                            $.ajax({
                                url: endereco + '/questoes',
                                data: {
                                    'id': $('#id').val(),
                                    'questoes': questoes
                                },
                                type: 'POST',
                                success: function (result) {

                                    removerNotificacoes();

                                    // se tiver sido OK
                                    if (result === 'ok') {
                                        notificarSucesso('<i class="icon-ok"></i> Vinculação efetuada com sucesso!');
                                    }
                                    // do contrário
                                    else {
                                        notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao vincular.');
                                    }
                                },
                                error: function (result) {
                                    notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao vincular.');
                                }
                            });
                        }
                    }
                }
            });
    });

    // aplica o tema para o painél de legenda
    themePanel('#simulados-legenda', 70, 50);
});

/**
 * Solicita a liberação ou não liberação do registro.
 */
function liberar(_liberar) {
    var simulados, item, value;

    simulados = $('#simulados');
    item = simulados.jqxListBox('getSelectedItem');
    value = item.value;

    notificarInformacao('<i class="icon-info-sign"></i> Atualizando situação...');
    $.ajax({
        url: endereco + '/liberar/' + value.id + '/' + (_liberar ? 'sim' : 'nao'),
        data: null,
        type: 'GET',
        success: function (result) {

            removerNotificacoes();

            if (result === 'ok') {
                notificarSucesso('<i class="icon-ok"></i> Situação atualizada!');

                // atualiza os itens
                simulados.jqxListBox('refresh');
            } else {
                notificarSucesso('<i class="icon-warning-sign"></i> Ocorreu um erro ao atualizar a situação.');
            }
        }
    });
}

/**
 * Solicita a confirmação de remoção do registro.
 */
function remover() {
    var item, value, janela;

    item = $('#simulados').jqxListBox('getSelectedItem');
    value = item.value;

    janela = $('<div id="#dialog-remover" />')
        .html('Deseja remover o simulado "' + value.id + ' - ' + value.Titulo + '"?')
        .dialog({
            modal: true,
            closeOnEscape: false,
            resizable: false,
            title: 'Confirmação',
            buttons: {
                'Não': function () {
                    $(this).dialog('close');

                    return false;
                },
                'Sim': function () {
                    $.ajax({
                        url: endereco,
                        data: {
                            'id': value.id
                        },
                        type: 'DELETE',
                        success: function (result) {
                            var simulados;

                            // fecha a janela
                            janela.dialog('close');

                            if (result === 'ok') {
                                // remove o item selecionado
                                simulados = $('#simulados');
                                simulados.jqxListBox('removeAt', simulados.jqxListBox('getSelectedIndex'));

                                // limpa o formulário / coloca uma ficha vazia
                                $('#adicionar').click();
                            }
                        }
                    });
                }
            },
            open: function () {
                // remove o botão de fechar
                $(this).parent().find('.ui-dialog-titlebar-close').remove();

                $('.ui-dialog-buttonpane button', $(this).parent()).each(function () {
                    $(this).addClass('btn');
                    if ($(this).text() === 'Sim') {
                        $(this).prepend('<i class="fam-tick"></i> ');
                    } else if ($(this).text() === 'Não') {
                        $(this).prepend('<i class="fam-cross"></i> ').focus();
                    }
                });
                //addClass('btn').focus();
            },
            close: function () {
                // destrói a janela
                $(janela).parent().remove();
                $(janela).remove();
                janela = null;
            }
        });
}

function selecionarAoAdicionar() {
    var id, simulados, total;

    // se veio de um processamento de salvar
    if (salvado) {
        id = $('#id').val();
        simulados = $('#simulados');
        total = simulados.jqxListBox('getItems').length;

        // se já existem valores e a identificação está vazia
        if ((total > 0) && (id <= 0)) {
            // seleciona o último item
            simulados.jqxListBox('selectIndex', (total - 1));
        }
        // não existem itens selecionados
        else if (simulados.jqxListBox('getSelectedIndex') < 0) {
            setTimeout('selecionarAoAdicionar()', 50);
        }
    }
}

function liberarFormulario(liberar) {
    $('#id').prop('disabled', !liberar);
    $('#titulo').prop('disabled', !liberar);
    $('#versao').prop('disabled', !liberar);
    $('#pontuacao').prop('disabled', !liberar);
    $('#duracao').prop('disabled', !liberar);
    $('#inicio').prop('disabled', !liberar);
    $('#fim').prop('disabled', !liberar);
    $('#exibirDescricaoInicio').prop('disabled', !liberar);
    $('#descricao').prop('disabled', !liberar);

    // se estiver liberado
    if (liberar) {
        // foca o campo título
        $('#titulo').focus();

        // habilita todos os spinners
        $('#versao, #pontuacao, #duracao').spinner('enable');

        // habilita o botão de salvar
        $('#salvar').removeClass('disabled');
    }
    // do contrário
    else {
        // desabilita todos os spinners
        $('#versao, #pontuacao, #duracao').spinner('disable');

        // desabilita o botão "Salvar"
        $('#salvar').addClass('disabled');
    }
}
</script>
<div>
    <div class="coluna esquerda">
        <input id="procurar" type="text" placeholder="Digite e aperte ENTER para procurar"/>
        <button id="adicionar" class="btn"><i class="fam-add"></i> Adicionar</button>
        <label>Simulado</label>

        <div id="simulados" class="clear"></div>
        <div id="simulados-legenda">
            <div>Legenda</div>
            <div>
                <ul>
                    <li><i class="fam-lock-open"></i> Em edição</li>
                    <li><i class="fam-lock"></i> Em execução</li>
                    <li><i class="fam-stop"></i> Finalizado</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="coluna conteudo">
        <form id="simulado">
            <fieldset>
                <legend>Propriedades</legend>
                <p>Nesta página você pode definir as propriedades principais e a descrição do simulado. A descrição será
                    exibida imediatamente antes do início do simulado e pode ser usada para informações adicionais.</p>

                <!-- Identificação -->
                <input id="id" type="hidden"/>

                <!-- Título -->
                <div class="campo full-width">
                    <label for="titulo" class="obrigatorio">Título</label>
                    <input id="titulo" name="titulo" data-titulo="Título" type="text"/>
                </div>

                <!-- Versão -->
                <div class="campo medium-width">
                    <label for="versao" class="obrigatorio">Versão</label>
                    <input id="versao" name="versao" data-titulo="Versão" type="text"/>
                </div>

                <!-- Cadastro -->
                <div class="campo medium-width">
                    <label for="cadastro">Criado em</label>
                    <input id="cadastro" type="text" disabled="true"/>
                </div>

                <!-- Pontuação -->
                <div class="campo medium-width">
                    <label for="pontuacao" class="obrigatorio">Pontuação Mínima</label>
                    <input id="pontuacao" name="pontuacao" data-titulo="Pontuação Mínima" type="text"/>
                </div>

                <!-- Duração -->
                <div class="campo">
                    <label for="duracao" class="obrigatorio">Duração (em minutos)</label>
                    <input id="duracao" name="duracao" data-titulo="Duração" type="text"/>
                </div>

                <!-- Data e Hora do Início e Data e Hora do Fim -->
                <div class="clear">
                    <label for="inicio" class="obrigatorio">Inicia em</label>
                    <input id="inicio" name="inicio" data-titulo="Data e Hora de Início" type="text"/>
                    <span>à</span>
                    <input id="fim" name="fim" data-titulo="Data e Hora do Término" type="text"/>
                </div>

                <!-- Descrição -->
                <div class="campo full-width">
                    <label for="exibirDescricaoInicio">Descrição
                        <input id="exibirDescricaoInicio" type="checkbox"/>
                        <span>Exibir a descrição no início do simulado</span></label>
                    <textarea id="descricao"></textarea>
                </div>

                <!-- Painél de Controle -->
                <div id="painel-controle">
                    <div class="direita">
                        <button id="salvar" class="btn"><i class="fam-disk"></i> Salvar</button>
                    </div>
                    <div class="esquerda">
                        <button id="turmas" class="btn"><i class="fam-group"></i> Turmas</button>
                        <button id="assuntos" class="btn"><i class="fam-table-multiple"></i> Assuntos</button>
                        <button id="questoes" class="btn"><i class="fam-help"></i> Questões</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<br class="clear"/>