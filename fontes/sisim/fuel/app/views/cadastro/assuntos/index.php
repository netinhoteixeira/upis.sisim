<script type="text/javascript">
var endereco, salvado;

endereco = '<?php Uri::base(); ?>/sisim/rest/cadastro/assuntos';
salvado = false;

/**
 * Inicialização do processamento.
 */
$(document).ready(function () {
    var origem, adaptadorDeDados;

    // popula os dados do combo Disciplina
    $.get(endereco + '/../disciplinas.json', null, function (data) {
        var i;

        for (i = 0; i < data.length; (i = i + 1)) {
            $('#disciplina').append('<option value="' + data[i].valor + '">' + data[i].rotulo + '</option>');
        }

        // deixa nenhuma disciplina selecionada
        $('#disciplina').val(new String());
    });

    // habilita a caixa de procura
    $('#procurar').enterKey(function () {
        // atualiza a listagem
        $('#assuntos').jqxListBox('refresh');
    });

    // prepara os dados
    origem =
    {
        datatype: 'json',
        datafields: [
            {
                name: 'rotulo'
            },
            {
                name: 'valor'
            }
        ],
        id: 'id',
        url: endereco + '.json'
    };
    adaptadorDeDados = new $.jqx.dataAdapter(origem, {
        loadServerData: function (serverdata, source, callback) {
            var procurarpor;

            procurarpor = $('#procurar').val().trim();

            if (procurarpor.length > 0) {
                serverdata = {
                    'procurarpor': procurarpor
                };
            }

            $.ajax({
                dataType: source.datatype,
                url: source.url,
                data: serverdata,
                success: function (data, status, xhr) {
                    var dataArray, i, record, datarow, datafield, value;

                    dataArray = new Array();

                    if (data !== undefined) {
                        for (i = 0; i < data.length; (i = i + 1)) {
                            record = data[i];
                            datarow = {};

                            datarow['rotulo'] = record.rotulo;
                            datarow['valor'] = record.valor;

                            dataArray[dataArray.length] = datarow;
                        }
                    }

                    // envia os registros carregados para o plugin jqxDataAdapter
                    callback({ records: dataArray });

                    setTimeout('selecionarAoAdicionar()', 50);
                    setTimeout('destacarProcuraNaListagem("procurar", "assuntos")', 50);
                }
            });
        }
    });

    // cria um jqxListBox
    $('#assuntos').jqxListBox({
        source: adaptadorDeDados,
        displayMember: 'rotulo',
        valueMember: 'valor',
        width: '100%',
        height: 303,
        theme: 'bootstrap'
    });
    $('#assuntos').on('select', function (event) {
        var item, value;

        if (event.args) {
            item = event.args.item;
            if (item) {
                value = item.value;

                $('#id').val(value.id);
                $('#nome').val(value.Nome);
                $('#disciplina').val(value.Disciplina);

                // DONE: Correção para a validação que ocorria quando o formulário ficava vazio e o campo de título
                // perdia o foco
                $('#nome').blur().focus();

                $('#painel-controle .esquerda').fadeIn('fast');

                removerNotificacoesErro();
            }
        }
    });

    // adiciona um asterisco vermelho para os campos obrigatórios
    definirComoObrigatorio();

    // define a validação do formulário
    $('#assunto').validate({
        rules: {
            'nome': 'required',
            'disciplina': 'required'
        },
        onfocusout: function (element) {
            this.element(element);
        },
        onkeyup: false,
        errorPlacement: function (error, element) {
            var existe, mensagem, mensagens, i;

            existe = false;
            mensagem = '<i class="icon-warning-sign"></i> ' + element.data('nome') + ': ' + error.text();

            // verifica se já existe uma notificação com o título
            mensagens = $('.bootstrap-growl').toArray();
            for (i = 0; i < mensagens.length; (i = i + 1)) {
                if ($(mensagens[i]).html() === mensagem) {
                    existe = true;
                    break;
                }
            }

            if (!existe) {
                notificarErro(mensagem);
            }
        },
        submitHandler: function (form) {
            var id, nome, disciplina, disciplinaNome;

            // remove todas as notificações
            $('.qtip').remove();

            // obtém os valores dos campos
            id = $('#id').val();
            nome = $('#nome').val();
            disciplina = $('#disciplina').val();
            disciplinaNome = $('#disciplina option:selected').text();

            // notifica ao formulário que ele está/foi salvo
            salvado = true;

            // solicita a inserção/alteração do registro
            notificarInformacao('<i class="icon-info-sign"></i> Salvando...');
            $.ajax({
                url: endereco,
                data: {
                    'id': id,
                    'nome': nome,
                    'disciplina': disciplina,
                    'disciplinaNome': disciplinaNome
                },
                type: (id.length === 0) ? 'POST' : 'PUT',
                success: function (result) {

                    removerNotificacoes();

                    // se tiver sido OK
                    if (result === 'ok') {
                        notificarSucesso('<i class="icon-ok"></i> Salvo com sucesso!');

                        // atualiza a lista de assuntos
                        $('#assuntos').jqxListBox('refresh');
                    }
                    // do contrário
                    else if (result === 'nok') {
                        notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao salvar.');
                    } else {
                        notificarErro('<i class="icon-warning-sign"></i> ' + result);
                    }
                },
                error: function (result) {
                    notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao salvar.');
                }
            });
        }
    });

    // ao clicar no botão "Adicionar"
    $('#adicionar').click(function () {
        removerNotificacoes();

        // deseleciona algum item da listagem
        $('#assuntos').jqxListBox('clearSelection');

        // limpa o formulário
        $('#id, #nome').val(new String());
        $('#cadastro').parent().hide();
        $('#disciplina').val(0);

        // define o foco para o campo Nome
        $('#nome').focus();
    }).click();

    // ao clicar no botão "Salvar"
    $('#salvar').click(function (e) {
        var janela;

        e.preventDefault();

        // previne continuar a ação caso o botão esteja desabilitado
        if ($(this).hasClass('disabled')) {
            return false;
        }

        janela = $('<div id="#dialog-salvar" />')
            .html('Deseja salvar?')
            .dialog({
                modal: true,
                closeOnEscape: false,
                resizable: false,
                title: 'Confirmação',
                buttons: {
                    'Não': function () {
                        $(this).dialog('close');

                        return false;
                    },
                    'Sim': function () {
                        var form;

                        $(this).dialog('close');

                        // valida os campos do formulário e caso seja válido, envia
                        form = $('#assunto');
                        if (form.valid()) {
                            form.submit();
                        }

                        return false;
                    }
                },
                open: function () {
                    // remove o botão de fechar
                    $(this).parent().find('.ui-dialog-titlebar-close').remove();

                    $('.ui-dialog-buttonpane button', $(this).parent()).each(function () {
                        $(this).addClass('btn');
                        if ($(this).text() === 'Sim') {
                            $(this).prepend('<i class="fam-tick"></i> ');
                        } else if ($(this).text() === 'Não') {
                            $(this).prepend('<i class="fam-cross"></i> ').focus();
                        }
                    });
                    //addClass('btn').focus();
                },
                close: function () {
                    // destrói a janela
                    $(janela).parent().remove();
                    $(janela).remove();
                    janela = null;
                }
            });
    });

    // ao clicar no botão "Disciplinas"
    $('#questoes').click(function (e) {
        // evita o formulário ser enviado, sendo que queremos adicionar outras funcionalidades
        e.preventDefault();

        //window.open('sisim/cadastro/questoes');
        window.open(<?php Uri::base(); ?>'/sisim/cadastro/questoes', '_self');
    });
});

/**
 * Solicita a confirmação de remoção do registro.
 */
function remover() {
    var item, value, janela;

    item = $('#assuntos').jqxListBox('getSelectedItem');
    value = item.value;

    janela = $('<div id="#dialog-remover" />')
        .html('Deseja remover o assunto "' + value.id + ' - ' + value.Nome + '"?')
        .dialog({
            modal: true,
            closeOnEscape: false,
            resizable: false,
            title: 'Confirmação',
            buttons: {
                'Não': function () {
                    $(this).dialog('close');

                    return false;
                },
                'Sim': function () {
                    $.ajax({
                        url: endereco,
                        data: {
                            'id': value.id
                        },
                        type: 'DELETE',
                        success: function (result) {
                            var assuntos;

                            // fecha a janela
                            janela.dialog('close');

                            if (result === 'ok') {
                                // remove o item selecionado
                                assuntos = $('#assuntos');
                                assuntos.jqxListBox('removeAt', assuntos.jqxListBox('getSelectedIndex'));

                                // limpa o formulário / coloca uma ficha vazia
                                $('#adicionar').click();
                            }
                        }
                    });
                }
            },
            open: function () {
                // remove o botão de fechar
                $(this).parent().find('.ui-dialog-titlebar-close').remove();

                $('.ui-dialog-buttonpane button', $(this).parent()).each(function () {
                    $(this).addClass('btn');
                    if ($(this).text() === 'Sim') {
                        $(this).prepend('<i class="fam-tick"></i> ');
                    } else if ($(this).text() === 'Não') {
                        $(this).prepend('<i class="fam-cross"></i> ').focus();
                    }
                });
                //addClass('btn').focus();
            },
            close: function () {
                // destrói a janela
                $(janela).parent().remove();
                $(janela).remove();
                janela = null;
            }
        });
}

function selecionarAoAdicionar() {
    var id, assuntos, total;

    // se veio de um processamento de salvar
    if (salvado) {
        id = $('#id').val();
        assuntos = $('#assuntos');
        total = assuntos.jqxListBox('getItems').length;

        // se já existem valores e a identificação está vazia
        if ((total > 0) && (id <= 0)) {
            // seleciona o último item
            assuntos.jqxListBox('selectIndex', (total - 1));
        }
        // não existem itens selecionados
        else if (assuntos.jqxListBox('getSelectedIndex') < 0) {
            setTimeout('selecionarAoAdicionar()', 50);
        }
    }
}
</script>
<div>
    <div class="coluna esquerda">
        <input id="procurar" type="text" placeholder="Digite e aperte ENTER para procurar"/>
        <button id="adicionar" class="btn"><i class="fam-add"></i> Adicionar</button>
        <label>Assunto</label>

        <div id="assuntos" class="clear"></div>
    </div>
    <div class="coluna conteudo">
        <form id="assunto">
            <fieldset>
                <legend>Propriedades</legend>
                <p>Nesta página você pode definir as propriedades principais e a descrição do assunto.</p>

                <!-- Identificação -->
                <input id="id" type="hidden"/>

                <!-- Título -->
                <div class="campo full-width">
                    <label for="nome" class="obrigatorio">Título</label>
                    <input id="nome" name="nome" data-nome="Título" type="text"/>
                </div>

                <!-- Disciplina -->
                <div class="campo full-width">
                    <label for="disciplina" class="obrigatorio">Disciplina</label>
                    <select id="disciplina" name="disciplina" data-nome="Disciplina"></select>
                </div>

                <!-- Painel de Controle -->
                <div id="painel-controle">
                    <div class="direita">
                        <button id="salvar" class="btn"><i class="fam-disk"></i> Salvar</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<br class="clear"/>