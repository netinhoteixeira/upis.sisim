<!--
<div class="hero-unit">
    <h1>Seja bem-vindo!</h1>
    <p>Vamos começar um simulado?</p>
    <p><a class="btn btn-primary btn-large" href="<?php Uri::base(); ?>/sisim/executar">Só se for agora</a></p>
</div>
-->
<div class="row">
    <div class="span4">
        <h2>Simples</h2>
        <p>Nada de muitas telinhas, campos e botões. É uma ferramenta bastante limpa para não confundir quem estiver usando.</p>
        <p>Isso não quer dizer que não seja uma ferramenta poderosa!</p>
    </div>
    <div class="span4">
        <h2>Fácil</h2>
        <p>O SiSim foi feito pensando em você!<br/>Não precisa ler um manual para usá-lo.</p>
        <p>Você mal olhou e sem perceber já está "pedalando".</p>
    </div>
    <div class="span4">
        <h2>Prático</h2>
        <p>Pra quê complicar?! Simples, fácil e prático!
        <br/>Poucos cliques, grandes resultados.</p>
    </div>
</div>