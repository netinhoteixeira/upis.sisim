<div class="row">
    <div class="span16">
        <h1><?php echo $title; ?>
            <small>Não deu para encontrar o que você queria...</small>
        </h1>
        <hr>
        <p>O que você estava procurando não pôde ser encontrado aqui. Mas não desista, tente novamente!</p>

        <p>Comece pelo começo e reveja seus passos. Se o erro persistir, entre em contato com suporte.</p>
    </div>
</div>