<?php
/**
 * Created by JetBrains PhpStorm.
 * User: francisco
 * Date: 08/05/13
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <link rel="shortcut icon" href="<?php echo Uri::base(); ?>assets/favicon.ico"/>

    <?php echo Asset::css('bootstrap.min.css'); ?>
    <?php echo Asset::css('bootstrap-responsive.min.css'); ?>

    <!-- FamFamFam Icons : http://www.plugolabs.com/custom-twitter-bootstrap-button-generator-with-famfamfam-icons/ -->
    <?php echo Asset::css('fam-icons.css'); ?>

    <!-- jQuery UI, jQuery UI Timepicker e jQWidgets -->
    <?php echo Asset::css('smoothness/jquery-ui-1.10.3.custom.min.css'); ?>
    <?php echo Asset::css('jquery-ui-timepicker-addon.css'); ?>
    <?php echo Asset::css('../js/vendor/jqwidgets/styles/jqx.base.css'); ?>
    <?php echo Asset::css('../js/vendor/jqwidgets/styles/jqx.bootstrap.css'); ?>
    <?php echo Asset::css('jquery.qtip.min.css'); ?>
    <?php echo Asset::css('DT_bootstrap.css'); ?>

    <!-- Principal -->
    <?php echo Asset::css('main.css'); ?>

    <!-- Scripts -->
    <?php echo Asset::js('vendor/jquery/jquery-2.0.2.min.js'); ?>
    <?php echo Asset::js('vendor/jquery/jquery.filter_input.js'); ?>
    <?php echo Asset::js('vendor/jquery/jquery.validate.js'); ?>
    <?php echo Asset::js('vendor/jquery/jquery.validate.messages_pt_BR.js'); ?>
    <?php echo Asset::js('vendor/jqueryui/jquery-ui-1.10.3.custom.min.js'); ?>
    <?php echo Asset::js('vendor/jqueryui/jquery.ui.datepicker-pt-BR.js'); ?>
    <?php echo Asset::js('vendor/jqueryui/jquery-ui-sliderAccess.js'); ?>
    <?php echo Asset::js('vendor/jqueryui/jquery-ui-timepicker-addon.js'); ?>
    <?php echo Asset::js('vendor/jqueryui/jquery-ui-timepicker-pt-BR.js'); ?>
    <?php echo Asset::js('vendor/jqwidgets/jqx-all.js'); ?>
    <?php echo Asset::js('vendor/jquery/jquery.qtip.min.js'); ?>
    <?php echo Asset::js('vendor/jquery/jquery.bootstrap-growl.min.js'); ?>
    <?php echo Asset::js('vendor/jquery/jquery.highlightRegex.min.js'); ?>
    <?php echo Asset::js('vendor/jquery/jquery.dataTables.min.js'); ?>
    <?php echo Asset::js('vendor/jquery/DT_bootstrap.js'); ?>

    <?php echo Asset::js('vendor/ckeditor/ckeditor.js'); ?>
    <!--
    <?php echo Asset::js('vendor/ckeditor/config.js'); ?>
    <?php echo Asset::js('vendor/ckeditor/lang/pt-br.js'); ?>
    -->
</head>
<body>
<div id="header">
    <div class="row">
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">

                    <?php
                    if (Auth::check()) {
                        ?>
                        <span class="username">
                            Olá, <?php echo Auth::get_screen_name(); ?>
                        </span>
                    <?php } else { ?>
                        <form action="<?php echo Uri::base() . 'acessar'; ?>" method="post" class="form-inline userlogin">
                            <input name="usuario" type="text" class="input-small" placeholder="Usuário">
                            <input name="senha" type="password" class="input-small" placeholder="Senha">
                            <button type="submit" class="btn">Acessar</button>
                        </form>
                    <?php } ?>

                    <?php

                    // Menu

                    // Top Menu
                    $top_menu = array(
                        'attr' => array('class' => 'nav'),
                        'items' => array(
                            array(
                                'name' => '<div id="logo"></div>',
                                'link' => array(
                                    'url' => ' ',
                                    'attr' => array('class' => 'brand'),
                                    'part' => true
                                )
                            ),
                            /*
                            array(
                                'name' => 'Sobre',
                                'link' => array(
                                    'url' => ' ',
                                    'attr' => array('class' => ''),
                                    'part' => true
                                )
                            ),*/
                        ),
                    );

                    if (Auth::check()) {

                        if (Auth::has_access('cadastro.read')) {
                            $top_menu['items'][] = array(
                                'name' => 'Cadastro <b class="caret"></b>',
                                'link' => array(
                                    'url' => '#',
                                    'attr' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                    'part' => true
                                ),
                                'attr' => array('class' => 'dropdown'),
                                'menu' => array(
                                    'attr' => array('class' => 'dropdown-menu'),
                                    'items' => array(
                                        array(
                                            'name' => 'Assuntos',
                                            'link' => 'cadastro/assuntos'
                                        ),
                                        array(
                                            'name' => 'Questões',
                                            'link' => 'cadastro/questoes'
                                        ),
                                        array(
                                            'name' => 'Simulados',
                                            'link' => 'cadastro/simulados'
                                        ),
                                    ),
                                )
                            );
                        }

                        if (Auth::has_access('relatorio.read')) {
                            $top_menu['items'][] = array(
                                'name' => 'Relatórios <b class="caret"></b>',
                                'link' => array(
                                    'url' => '#',
                                    'attr' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                    'part' => true
                                ),
                                'attr' => array('class' => 'dropdown'),
                                'menu' => array(
                                    'attr' => array('class' => 'dropdown-menu'),
                                    'items' => array(
                                        array(
                                            'name' => 'Simulados',
                                            'link' => 'relatorio/simulados'
                                        ),
                                        array(
                                            'name' => 'Turmas',
                                            'link' => 'relatorio/turmas'
                                        ),
                                        array(
                                            'name' => 'Alunos',
                                            'link' => 'relatorio/alunos'
                                        ),
                                    ),
                                )
                            );
                        }

                        if (Auth::has_access('executar_simulado.read')) {
                            $top_menu['items'][] = array(
                                'name' => 'Executar Simulado',
                                'link' => array(
                                    'url' => 'executar',
                                    'attr' => array('class' => ''),
                                    'part' => true
                                )
                            );
                        }

                        $top_menu['items'][] = array(
                            'name' => 'Sair',
                            'link' => array(
                                'url' => '/sair',
                                'attr' => array('class' => ''),
                                'part' => true
                            )
                        );
                    }

                    Menu::forge('top_menu', $top_menu);

                    echo Menu::instance('top_menu')->get();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <?php //echo Breadcrumb::create_links(); ?>
    <h4><?php echo $title; ?></h4>

    <?php echo $content; ?>

    <hr/>
    <footer>
        <!-- <p class="pull-right">Página renderizada em {exec_time}s usando {mem_usage}mb de memória.</p> -->
        <p class="pull-right right">Desenvolvido pelos alunos <a href="mailto:fco.ernesto@gmail.com?subject=sisim">Francisco
                Ernesto</a>, <a href="mailto:jadypbs@gmail.com?subject=sisim">Jady Pâmella</a> e <a
                href="mailto:jhonatanmelo22@gmail.com?subject=sisim">Jhonatan Ferreira</a>.<br/>Com orientação da
            professora <a href="mailto:anapaulafukuda@gmail.com?subject=sisim">Ms. Ana Paula Fukuda</a>.</p>

        <p>&copy; SiSim - Sistema de Simulados<br/>Todos os direitos reservados.</p>
    </footer>

    <?php echo Asset::js('bootstrap.min.js'); ?>

    <?php echo Asset::js('main.js'); ?>
</div>
</body>
</html>
